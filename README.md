This started as the probably-reusable bits of [https://gitlab.com/Absotively/gale-sphere-2-v2](https://gitlab.com/Absotively/gale-sphere-2-v2), pulled out into a separate repository. It's grown a bit to add probably-reusable bits needed for [https://gitlab.com/Absotively/levinstrike](https://gitlab.com/Absotively/levinstrike).

If you have node installed, you can start a server for the samples by running `npm start` after you've run `npm install`.

The template directory provides a basic template/skeleton for a new sim.

Note that I am both too lazy to publish this in the npm repository and too lazy to remember to update the version number in package.json. Luckily for all of us, npm seems to handle dependencies that are git repos by recording the commit hash, so it more-or-less works out.

## License

This library is licensed under the MIT license.

I have put a copy of the license in a @license comment in engine.js; currently if you use the build setup from the template directory, and you use the Sim object, the license will be included in the minified JS without any effort on your part. So far as I am concerned this satisfies the requirement to include the license with "copies or substantial portions of the Software". (I say currently only because rollup could in theory change how it handles such things, though I don't know why they would.)

I definitely don't consider that requirement to apply to the contents of the template directory or anything derived from it, as it's not a substantial portion of the software, it's just an example/template. Do what you like with it.
