class Entity {
  constructor() {}
  update(sim, msSinceLastUpdate) {}
  addToDrawing(drawing) {}
  removeFromDrawing() {}
  allowMovement(from, to) {
    return true;
  }
}

class Entities {
  #entities;
  #sim;

  constructor(sim) {
    this.#entities = [];
    this.#sim = sim;
  }
  add(entity) {
    this.#entities.push(entity);
    entity.addToDrawing(this.#sim.drawing);
    return this.#entities.length - 1;
  }
  remove(entity) {
    for (let [index, value] of this.#entities.entries()) {
      if (Object.is(entity, value)) {
        this.#entities[index] = null;
      }
    }
    entity.removeFromDrawing(this.#sim.drawing);
  }
  update(sim, msSinceLastUpdate) {
    for (let e of this.#entities) {
      if (e && e.update) {
        e.update(sim, msSinceLastUpdate);
      }
    }
  }
  allowMovement(from, to) {
    for (let e of this.#entities) {
      if (e && !e.allowMovement(from, to)) {
        return false;
      }
    }
    return true;
  }
}

export { Entity, Entities };
