// inspired by a very half-assed skimming of the Phaser3 timeline docs
import { Entity } from "./entities.js";

class Timeline extends Entity {
  #events;
  #nextEvent;
  #time;
  #running;

  constructor() {
    super();
    this.#events = [];
    this.#nextEvent = null;
    this.#time = 0;
    this.#running = false;
  }
  addEvent(milliseconds, callback) {
    this.#events.push({ time: milliseconds, fn: callback });
    if (this.#running) {
      this.#sortEvents();
      if (milliseconds <= this.#time) {
        callback();
        this.#nextEvent++;
      }
    }
  }
  #sortEvents() {
    this.#events.sort((a, b) => a.time - b.time);
  }
  start() {
    if (this.#events.length > 0) {
      this.#sortEvents();
      this.#nextEvent = 0;
      this.#time = 0;
      this.#running = true;
    }
  }
  pause() {
    this.#running = false;
  }
  update(sim, msSinceLastUpdate) {
    if (this.#running) {
      this.#time += msSinceLastUpdate;
      while (
        this.#nextEvent < this.#events.length &&
        this.#events[this.#nextEvent].time <= this.#time
      ) {
        this.#events[this.#nextEvent].fn();
        this.#nextEvent++;
      }
      if (this.#nextEvent >= this.#events.length) {
        this.#running = false;
      }
    }
  }
}

export { Timeline };
