import { Entity } from "./entities.js";
import { createSVGElement } from "./drawing.js";
import { distance } from "./utilities.js";

class SquareArena extends Entity {
  #size;
  #color;
  #left;
  #right;
  #top;
  #bottom;
  #rect;

  constructor(sim, size, color) {
    super(-1);
    this.#size = size;
    this.#color = color;

    this.#left = 100 - size / 2;
    this.#right = 100 + size / 2;
    this.#top = 100 - size / 2;
    this.#bottom = 100 + size / 2;

    this.#rect = null;
  }

  update(sim, msSinceLastUpdate) {
    for (let pm of sim.party.list) {
      if (
        pm.x < this.#left ||
        pm.x > this.#right ||
        pm.y < this.#top ||
        pm.y > this.#bottom
      ) {
        pm.ko();
      }
    }
  }

  addToDrawing(drawing) {
    this.#rect = createSVGElement("rect");
    this.#rect.setAttribute("x", `${this.#left}`);
    this.#rect.setAttribute("y", `${this.#top}`);
    this.#rect.setAttribute("width", `${this.#size}`);
    this.#rect.setAttribute("height", `${this.#size}`);
    this.#rect.setAttribute("fill", this.#color);
    drawing.layers.arena.append(this.#rect);

    drawing.arenaClipPath.append(this.#rect.cloneNode());
  }

  removeFromDrawing() {
    if (this.#rect) {
      this.#rect.remove();
      this.#rect = null;
      drawing.arenaClipPath.replaceChildren();
    }
  }
}

class RoundArena extends Entity {
  #radius;
  #color;
  #circle;

  constructor(sim, size, color) {
    super(-1);
    this.#radius = size / 2;
    this.#color = color;

    this.#circle = null;
  }

  update(sim, msSinceLastUpdate) {
    for (let pm of sim.party.list) {
      if (distance(pm, { x: 100, y: 100 }) > this.#radius) {
        pm.ko();
      }
    }
  }

  addToDrawing(drawing) {
    this.#circle = createSVGElement("circle");
    this.#circle.setAttribute("cx", "100");
    this.#circle.setAttribute("cy", "100");
    this.#circle.setAttribute("r", `${this.#radius}`);
    this.#circle.setAttribute("fill", this.#color);
    drawing.layers.arena.append(this.#circle);

    drawing.arenaClipPath.append(this.#circle.cloneNode());
  }

  removeFromDrawing() {
    if (this.#circle) {
      this.#circle.remove();
      this.#circle = null;
      drawing.arenaClipPath.replaceChildren();
    }
  }
}

export { SquareArena, RoundArena };
