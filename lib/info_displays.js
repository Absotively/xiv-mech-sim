import { createSVGElement } from "./drawing.js";
import { Entity } from "./entities.js";
import { setAttributes } from "./utilities.js";

const font = "sans-serif";

class Hitbox extends Entity {
  #size;
  #location;
  #angle;
  #path;

  constructor(
    outerRingSize,
    location = { x: 100, y: 100 },
    degreesCWFromN = 0,
  ) {
    super();
    this.#size = outerRingSize;
    this.#location = location;
    this.#angle = degreesCWFromN;
    this.#path = null;
  }

  setLocation(point) {
    this.#location = point;
    this.#setPathTransform();
  }

  setDegreesCWFromN(degrees) {
    this.#angle = degrees;
    this.#setPathTransform();
  }

  addToDrawing(drawing) {
    this.#path = createSVGElement("path");
    this.#path.setAttribute(
      "d",
      "m 0.47070313,-0.01757813 -0.0234375,0.02343751 0.005859,0.0078125 0.015625,-0.01757813 0.0175781,0.01757813 0.007813,-0.0078125 z m -0.94140626,0 -0.0234375,0.02343751 0.007813,0.0078125 0.015625,-0.01562501 0.0175781,0.01562501 0.005859,-0.0078125 z m 0.46783204,-0.42578125 C -0.24644716,-0.44181725 -0.44335937,-0.24393972 -0.44335938,0 c 0,0.12244817 0.0496161,0.23328794 0.12984376,0.31351562 l 0.005332,-0.005332 C -0.38693105,0.22943614 -0.43554687,0.12055663 -0.43554688,0 c 0,-0.24111327 0.19443361,-0.43554687 0.43554688,-0.43554688 0.24111327,0 0.43554687,0.19443361 0.43554688,0.43554688 0,0.12055663 -0.0486158,0.22943614 -0.12736329,0.30818359 l 0.005332,0.005332 C 0.39374331,0.23328793 0.44335937,0.12244817 0.44335938,0 0.44335938,-0.24489635 0.24489635,-0.44335937 0,-0.44335938 c -9.5662635e-4,0 -0.00191589,-6.04e-6 -0.00287109,0 z M 0,-0.50585938 c -0.27884402,0 -0.50585938,0.22701536 -0.50585938,0.50585938 0,0.13942201 0.0567502,0.26589079 0.14835938,0.3575 l 0.007891,-0.007891 C -0.43896446,0.26025429 -0.49414062,0.13671981 -0.49414062,0 c 0,-0.27343963 0.22070099,-0.49414059 0.49414062,-0.49414062 0.27343963,3e-8 0.49414062,0.22070099 0.49414062,0.49414062 0,0.13671981 -0.0551762,0.26025429 -0.14453124,0.34960938 L 0.3575,0.3575 C 0.44910921,0.26589079 0.50585938,0.13942201 0.50585938,0 0.50585938,-0.27884402 0.27884402,-0.50585938 0,-0.50585938 Z M 0,-0.56 a 0.04,0.06 0 0 0 -0.04,0.06 0.04,0.06 0 0 0 1.953e-5,0.001602 A 0.5,0.5 0 0 1 0,-0.5 0.5,0.5 0 0 1 0.03998047,-0.49839844 0.04,0.06 0 0 0 0.04,-0.5 0.04,0.06 0 0 0 0,-0.56 Z",
    );
    this.#path.setAttribute("fill", "#ff1010");
    this.#path.setAttribute("stroke", "none");
    this.#setPathTransform();
    drawing.layers.enemies.append(this.#path);
  }

  removeFromDrawing() {
    if (this.#path) {
      this.#path.remove();
      this.#path = null;
    }
  }

  #setPathTransform() {
    if (this.#path) {
      this.#path.setAttribute(
        "transform",
        `translate(${this.#location.x},${this.#location.y}) rotate(${
          this.#angle
        }) scale(${this.#size})`,
      );
    }
  }
}

class Castbar extends Entity {
  #text;
  #duration;
  #time;
  #amountFilled;
  #width;
  #left;
  #svgGroup;
  #fillRect;

  constructor(text, duration) {
    super(0);
    this.#text = text;
    this.#duration = duration;
    this.#time = 0;
    this.#amountFilled = 0;
    this.#width = 0;
    this.#left = 0;
    this.#svgGroup = null;
    this.#fillRect = null;
  }

  update(sim, msSinceLastUpdate) {
    if (this.#svgGroup !== null) {
      this.#time += msSinceLastUpdate;
      if (this.#time >= this.#duration) {
        this.#amountFilled = 1;
      } else {
        this.#amountFilled = this.#time / this.#duration;
      }
      this.#fillRect.setAttribute(
        "width",
        `${this.#width * this.#amountFilled}`,
      );
    }
  }

  addToDrawing(drawing) {
    this.#width = 0.75 * drawing.size;
    this.#left = 100 - this.#width / 2;
    this.#svgGroup = createSVGElement("g");
    let text = createSVGElement("text");
    text.textContent = this.#text;
    text.setAttribute("x", `${this.#left}`);
    text.setAttribute("y", `${97 + drawing.size / 2}`);
    text.setAttribute("fill", "#fff");
    text.setAttribute("font-family", font);
    text.setAttribute("font-size", "1.5");
    this.#svgGroup.append(text);
    let rectTop = `${98 + drawing.size / 2}`;
    let rectHeight = "1";
    this.#fillRect = createSVGElement("rect");
    this.#fillRect.setAttribute("x", `${this.#left}`);
    this.#fillRect.setAttribute("y", rectTop);
    this.#fillRect.setAttribute("width", "0");
    this.#fillRect.setAttribute("height", rectHeight);
    this.#fillRect.setAttribute("fill", "rgb(255,189,57)");
    this.#svgGroup.append(this.#fillRect);
    let outlineRect = this.#fillRect.cloneNode();
    outlineRect.setAttribute("width", `${this.#width}`);
    outlineRect.setAttribute("fill", "rgba(0,0,0,0)");
    outlineRect.setAttribute("stroke-width", "0.2");
    outlineRect.setAttribute("stroke", "#fff");
    this.#svgGroup.append(outlineRect);
    drawing.layers.messages.append(this.#svgGroup);
  }

  removeFromDrawing() {
    if (this.#svgGroup !== null) {
      this.#svgGroup.remove();
      this.#svgGroup = null;
      this.#fillRect = null;
    }
  }
}

class BossSpeech extends Entity {
  #text;
  #svgText;

  constructor(text) {
    super();
    this.#text = text;
    this.#svgText = null;
  }

  addToDrawing(drawing) {
    this.#svgText = createSVGElement("text");
    this.#svgText.textContent = this.#text;
    this.#svgText.setAttribute("text-anchor", "middle");
    this.#svgText.setAttribute("x", "100");
    this.#svgText.setAttribute("y", `${104 - drawing.size / 2}`);
    this.#svgText.setAttribute("fill", "#fff");
    this.#svgText.setAttribute("font-family", font);
    this.#svgText.setAttribute("font-size", "2");
    drawing.layers.messages.append(this.#svgText);
  }

  removeFromDrawing() {
    if (this.#svgText !== null) {
      this.#svgText.remove();
      this.#svgText = null;
    }
  }
}

class Success extends Entity {
  #svgGroup;
  constructor() {
    super();
    this.#svgGroup = null;
  }

  addToDrawing(drawing) {
    this.#svgGroup = createSVGElement("g");
    let rect = createSVGElement("rect");
    this.#svgGroup.append(rect);
    let text = createSVGElement("text");
    text.textContent = "Success!";
    setAttributes(text, {
      "text-anchor": "middle",
      "dominant-baseline": "central",
      x: "100",
      y: "100",
      fill: "#fff",
      "font-family": font,
      "font-size": "5",
    });
    this.#svgGroup.append(text);
    drawing.layers.messages.append(this.#svgGroup);
    let paddingX = 1.3;
    let paddingY = 0.6;
    let textBBox = text.getBBox();
    setAttributes(rect, {
      x: `${textBBox.x - paddingX}`,
      y: `${textBBox.y - paddingY}`,
      width: `${textBBox.width + 2 * paddingX}`,
      height: `${textBBox.height + 2 * paddingY}`,
      fill: "rgba(0,0,0,0.8)",
      rx: "1",
      ry: "1",
      filter: "blur(0.1)",
    });
  }

  removeFromDrawing() {
    if (this.#svgGroup !== null) {
      this.#svgGroup.remove();
      this.#svgGroup = null;
    }
  }
}

class RestartInfo extends Entity {
  #backgroundColor = null;
  #svgGroup;

  constructor(backgroundColor) {
    super(1);
    this.#backgroundColor = backgroundColor;
    this.#svgGroup = null;
  }

  addToDrawing(drawing) {
    this.#svgGroup = createSVGElement("g");
    if (this.#backgroundColor !== null) {
      let bg = createSVGElement("rect");
      bg.setAttribute("x", `${100 - drawing.size / 2}`);
      bg.setAttribute("y", `${95 + drawing.size / 2}`);
      bg.setAttribute("width", `${drawing.size}`);
      bg.setAttribute("height", "6");
      bg.setAttribute("fill", this.#backgroundColor);
      this.#svgGroup.append(bg);
    }
    let restart = createSVGElement("text");
    restart.textContent = "Press SPACE or ENTER or gamepad A to restart";
    restart.setAttribute("text-anchor", "middle");
    restart.setAttribute("x", "100");
    restart.setAttribute("y", `${97 + drawing.size / 2}`);
    restart.setAttribute("fill", "#fff");
    restart.setAttribute("font-family", font);
    restart.setAttribute("font-size", "1.5");
    this.#svgGroup.append(restart);
    let menu = restart.cloneNode();
    menu.textContent =
      "Press ESC or the gamepad menu button to change settings";
    menu.setAttribute("y", `${99 + drawing.size / 2}`);
    this.#svgGroup.append(menu);
    drawing.layers.messages.append(this.#svgGroup);
  }

  removeFromDrawing() {
    if (this.#svgGroup !== null) {
      this.#svgGroup.remove();
      this.#svgGroup = null;
    }
  }
}

// 6, 7 and 8 will be [width] wide, base of most will be [bottomOffset] above target center,
// dots will be [width]/8 in diameter
class LCNumbers extends Entity {
  #targetsToNumbersMap;
  #width;
  #circleSize;
  #usualSpacing;
  #bottomLineCentresOffset;
  #svgGroupsToTargetsMap;
  constructor(targetsToNumbersMap, width = 3.2, bottomOffset = 1.6) {
    super(1);
    this.#targetsToNumbersMap = targetsToNumbersMap;
    this.#width = width;
    this.#circleSize = this.#width / 8;
    this.#usualSpacing = (this.#width - this.#circleSize) / 3;
    this.#bottomLineCentresOffset = -1 * bottomOffset - this.#circleSize / 2;
    this.#svgGroupsToTargetsMap = new Map();
  }

  update() {
    this.#svgGroupsToTargetsMap.forEach((target, group, map) => {
      group.setAttribute("transform", `translate(${target.x},${target.y})`);
    });
  }

  addToDrawing(drawing) {
    this.#targetsToNumbersMap.forEach((number, target, map) => {
      let circleLocations = null;
      let bottomY = this.#bottomLineCentresOffset;
      let topY = bottomY - this.#usualSpacing;
      let middleY = bottomY - this.#usualSpacing / 2;
      switch (number) {
        case 1:
          circleLocations = [{ x: 0, y: middleY }];
          break;
        case 2:
          circleLocations = [
            { x: 0 - this.#usualSpacing / 2, y: middleY },
            { x: this.#usualSpacing / 2, y: middleY },
          ];
          break;
        case 3:
          circleLocations = [
            { x: 0 - this.#usualSpacing / 2, y: bottomY },
            { x: this.#usualSpacing / 2, y: bottomY },
            { x: 0, y: topY },
          ];
          break;
        case 4:
          circleLocations = [
            { x: 0 - this.#usualSpacing / 2, y: bottomY },
            { x: this.#usualSpacing / 2, y: bottomY },
            { x: 0 - this.#usualSpacing / 2, y: topY },
            { x: this.#usualSpacing / 2, y: topY },
          ];
          break;
        case 5:
          circleLocations = [
            { x: 0 - this.#usualSpacing / 2, y: middleY },
            { x: this.#usualSpacing / 2, y: topY },
            { x: this.#usualSpacing / 2, y: bottomY },
            { x: (this.#usualSpacing * 3) / 2, y: topY },
            { x: (this.#usualSpacing * 3) / 2, y: bottomY },
          ];
          break;
        case 6:
          circleLocations = [
            { x: 0 - this.#usualSpacing, y: topY },
            { x: 0 - (this.#usualSpacing * 3) / 2, y: bottomY },
            { x: 0 - this.#usualSpacing / 2, y: bottomY },
            { x: this.#usualSpacing, y: bottomY },
            { x: (this.#usualSpacing * 3) / 2, y: topY },
            { x: this.#usualSpacing / 2, y: topY },
          ];
          break;
        case 7:
          circleLocations = [
            { x: 0 - this.#usualSpacing, y: topY },
            { x: 0 - (this.#usualSpacing * 3) / 2, y: bottomY },
            { x: 0 - this.#usualSpacing / 2, y: bottomY },
            { x: (this.#usualSpacing * 3) / 2, y: topY },
            { x: this.#usualSpacing / 2, y: topY },
            { x: (this.#usualSpacing * 3) / 2, y: bottomY },
            { x: this.#usualSpacing / 2, y: bottomY },
          ];
          break;
        case 8:
          circleLocations = [
            { x: 0 - (this.#usualSpacing * 3) / 2, y: topY },
            { x: 0 - this.#usualSpacing / 2, y: topY },
            { x: 0 - (this.#usualSpacing * 3) / 2, y: bottomY },
            { x: 0 - this.#usualSpacing / 2, y: bottomY },
            { x: (this.#usualSpacing * 3) / 2, y: topY },
            { x: this.#usualSpacing / 2, y: topY },
            { x: (this.#usualSpacing * 3) / 2, y: bottomY },
            { x: this.#usualSpacing / 2, y: bottomY },
          ];
          break;
      }
      let circleColor = number % 2 ? "#dae8fc" : "#ffeaff";
      let circleOutline = number % 2 ? "#97c1fc" : "#fcaefc";
      let group = createSVGElement("g");
      for (const point of circleLocations) {
        let circle = createSVGElement("circle");
        circle.setAttribute("cx", point.x);
        circle.setAttribute("cy", point.y);
        circle.setAttribute("r", this.#circleSize);
        circle.setAttribute("fill", circleColor);
        circle.setAttribute("stroke-width", this.#circleSize / 8);
        circle.setAttribute("stroke", circleOutline);
        group.append(circle);
      }
      group.setAttribute("transform", `translate(${target.x},${target.y})`);
      drawing.layers.partyMemberEffects.append(group);
      this.#svgGroupsToTargetsMap.set(group, target);
    });
  }

  removeFromDrawing() {
    this.#svgGroupsToTargetsMap.forEach((target, group, map) => {
      group.remove();
    });
    this.#svgGroupsToTargetsMap.clear();
  }
}

class BlueMarker extends Entity {
  #svgGroup;
  #svgGroupVerticalOffset;

  constructor(target, verticalOffset = 1.6) {
    super(1);
    this.target = target;
    this.#svgGroupVerticalOffset = verticalOffset;
    this.#svgGroup = null;
  }

  update() {
    if (this.#svgGroup !== null) {
      this.#svgGroup.setAttribute(
        "transform",
        `translate(${this.target.x},${this.target.y})`,
      );
    }
  }

  addToDrawing(drawing) {
    this.#svgGroup = createSVGElement("g");
    let dot = createSVGElement("circle");
    const dotRadius = 0.5;
    let centerY = `${-1 * dotRadius - this.#svgGroupVerticalOffset}`;
    dot.setAttribute("cx", "0");
    dot.setAttribute("cy", `${centerY}`);
    dot.setAttribute("r", `${dotRadius}`);
    dot.setAttribute("fill", "#092459");
    this.#svgGroup.append(dot);
    let ring = createSVGElement("circle");
    ring.setAttribute("cx", "0");
    ring.setAttribute("cy", centerY);
    ring.setAttribute("r", "0.3");
    ring.setAttribute("stroke-width", "0.1");
    ring.setAttribute("stroke", "#97dfff");
    this.#svgGroup.append(ring);
    this.#svgGroup.setAttribute(
      "transform",
      `translate(${this.target.x},${this.target.y})`,
    );
    drawing.layers.partyMemberEffects.append(this.#svgGroup);
  }

  removeFromDrawing() {
    if (this.#svgGroup !== null) {
      this.#svgGroup.remove();
      this.#svgGroup = null;
    }
  }

  draw() {
    let radius = 0.4;
    drawCircle(
      this.target.x,
      this.target.y - this.verticalOffset - radius,
      2 * radius,
      "#97dfff",
      "#092459",
    );
  }
}

export {
  Hitbox,
  Castbar,
  BossSpeech,
  Success,
  RestartInfo,
  LCNumbers,
  BlueMarker,
};
