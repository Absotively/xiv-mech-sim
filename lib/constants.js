const theoreticalTicksPerSecond = 60;

const partyMemberSpeedInYalmsPerSecond = 6;

const standardWarningColor = "rgba(255,85,0,0.7)";

export {
  theoreticalTicksPerSecond,
  partyMemberSpeedInYalmsPerSecond,
  standardWarningColor,
};
