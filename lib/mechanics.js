import { Entity } from "./entities.js";
import { createSVGElement } from "./drawing.js";
import { standardWarningColor } from "./constants.js";
import { distance } from "./utilities.js";

class Stacks extends Entity {
  #circles;
  constructor(options) {
    const defaults = {
      hit_color: "rgba(5,5,43,0.7)",
    };
    let resolvedOptions = { ...defaults, ...options };
    super(0);
    this.targets = resolvedOptions.targets;
    this.soak_count = resolvedOptions.soak_count;
    this.radius = resolvedOptions.radius;
    this.color = resolvedOptions.color;
    this.hit_color = resolvedOptions.hit_color;
    this.hasHit = false;
    this.#circles = [];
  }

  hit(partyList) {
    let hit = [];
    let newFakeTargets = [];
    for (let target of this.targets) {
      let newFakeTarget = { x: target.x, y: target.y };
      let in_stack = [];
      for (let pm of partyList) {
        if (pm.isActive && distance(pm, newFakeTarget) < this.radius) {
          in_stack.push(pm);
          if (hit.includes(pm)) {
            pm.ko();
          } else {
            hit.push(pm);
          }
        }
      }
      if (in_stack.length < this.soak_count) {
        for (let pm of in_stack) {
          pm.ko();
        }
      }
      newFakeTargets.push(newFakeTarget);
    }
    this.targets = newFakeTargets;
    for (let circle of this.#circles) {
      circle.setAttribute("fill", this.hit_color);
      circle.setAttribute("stroke", "transparent");
    }
    this.hasHit = true;
  }

  update() {
    let i = 0;
    for (let target of this.targets) {
      this.#circles[i].setAttribute("cx", `${target.x}`);
      this.#circles[i].setAttribute("cy", `${target.y}`);
      i++;
    }
  }

  addToDrawing(drawing) {
    let color = this.color;
    let fill = "transparent";
    if (this.hasHit) {
      fill = this.hit_color;
      color = "transparent";
    }
    for (const target of this.targets) {
      let circle = createSVGElement("circle");
      circle.setAttribute("cx", `${target.x}`);
      circle.setAttribute("cy", `${target.y}`);
      circle.setAttribute("r", `${this.radius}`);
      circle.setAttribute("fill", fill);
      circle.setAttribute("stroke-width", 0.25);
      circle.setAttribute("stroke", color);
      this.#circles.push(circle);
      drawing.layers.arenaClippedMechanics.append(circle);
    }
  }

  removeFromDrawing() {
    for (let circle of this.#circles) {
      circle.remove();
    }
    this.#circles = [];
  }
}

class LPStacks extends Stacks {
  constructor(sim) {
    let targets = [];
    for (let pm of sim.party.list) {
      if (pm.is_ranged && !pm.is_dps) {
        targets.push(pm);
      }
    }
    super({
      targets: targets,
      soak_count: 4,
      radius: 6,
      color: "rgb(255,255,28)",
    });
  }
}

class Enums extends Stacks {
  constructor(sim, target_dps) {
    let targets = [];
    for (let pm of sim.party.list) {
      if (target_dps == pm.is_dps) {
        targets.push(pm);
      }
    }
    super({
      targets: targets,
      soak_count: 2,
      radius: 3,
      color: "#fff",
    });
  }
}

// if the target is a party member, that party member survives
// any non-target party members in the AoE do not
class CircleAoE extends Entity {
  #circle;

  constructor(options) {
    const defaults = {
      hit_color: "rgba(5,5,43,0.7)",
      warning_color: standardWarningColor,
    };
    let resolvedOptions = { ...defaults, ...options };
    super(0);
    this.target = resolvedOptions.target;
    this.radius = resolvedOptions.radius;
    this.warning_color = resolvedOptions.warning_color;
    this.hit_color = resolvedOptions.hit_color;
    this.has_hit = false;
    this.#circle = null;
  }

  hit(partyList) {
    for (let pm of partyList) {
      if (Object.is(pm, this.target)) {
        continue;
      }
      if (distance(pm, this.target) <= this.radius) {
        pm.ko();
      }
    }
    this.has_hit = true;
    if (this.#circle !== null) {
      this.#circle.setAttribute("fill", this.hit_color);
    }
    // hit effect should be stationary
    this.target = { x: this.target.x, y: this.target.y };
  }

  update() {
    if (this.#circle !== null) {
      this.#circle.setAttribute(
        "transform",
        `translate(${this.target.x},${this.target.y})`,
      );
    }
  }

  addToDrawing(drawing) {
    this.#circle = createSVGElement("circle");
    this.#circle.setAttribute("cx", "0");
    this.#circle.setAttribute("cy", "0");
    this.#circle.setAttribute("r", `${this.radius}`);
    this.#circle.setAttribute(
      "fill",
      this.has_hit ? this.hit_color : this.warning_color,
    );
    this.#circle.setAttribute(
      "transform",
      `translate(${this.target.x},${this.target.y})`,
    );
    drawing.layers.arenaClippedMechanics.append(this.#circle);
  }

  removeFromDrawing() {
    if (this.#circle !== null) {
      this.#circle.remove();
      this.#circle = null;
    }
  }
}

class DonutAoE extends Entity {
  #path;

  constructor(options) {
    const defaults = {
      hit_color: "rgba(5,5,43,0.7)",
      warning_color: standardWarningColor,
    };
    let resolvedOptions = { ...defaults, ...options };
    super(0);
    this.target = resolvedOptions.target;
    this.innerRadius = resolvedOptions.inner_radius;
    this.outerRadius = resolvedOptions.outer_radius;
    this.warning_color = resolvedOptions.warning_color;
    this.hit_color = resolvedOptions.hit_color;
    this.has_hit = false;
    this.#path = null;
  }

  hit(partyList) {
    for (let pm of partyList) {
      if (Object.is(pm, this.target)) {
        continue;
      }
      let d = distance(pm, this.target);
      if (d > this.innerRadius && d <= this.outerRadius) {
        pm.ko();
      }
    }
    this.has_hit = true;
    if (this.#path !== null) {
      this.#path.setAttribute("fill", this.hit_color);
    }
    // hit effect should be stationary
    this.target = { x: this.target.x, y: this.target.y };
  }

  update() {
    if (this.#path !== null) {
      this.#path.setAttribute(
        "transform",
        `translate(${this.target.x},${this.target.y})`,
      );
    }
  }

  addToDrawing(drawing) {
    this.#path = createSVGElement("path");
    this.#path.setAttribute(
      "d",
      `M0,-${this.outerRadius}
    A${this.outerRadius} ${this.outerRadius} 0 0 1 0,${this.outerRadius}
    A${this.outerRadius} ${this.outerRadius} 0 0 1 0,-${this.outerRadius}
    M0,-${this.innerRadius}
    A${this.innerRadius} ${this.innerRadius} 0 0 0 0,${this.innerRadius}
    A${this.innerRadius} ${this.innerRadius} 0 0 0 0,-${this.innerRadius}`,
    );
    this.#path.setAttribute(
      "fill",
      this.has_hit ? this.hit_color : this.warning_color,
    );
    this.#path.setAttribute(
      "transform",
      `translate(${this.target.x},${this.target.y})`,
    );
    drawing.layers.arenaClippedMechanics.append(this.#path);
  }

  removeFromDrawing() {
    if (this.#path !== null) {
      this.#path.remove();
      this.#path = null;
    }
  }
}

export { Stacks, LPStacks, Enums, CircleAoE, DonutAoE };
