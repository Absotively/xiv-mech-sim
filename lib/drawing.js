/* we use a similar coordinate system to fflogs' replay: coordinates are in yalms,
   and (100,100) is at the centre of the canvas */

import { newID, setAttributes } from "./utilities.js";

class Drawing {
  constructor(elementId, drawingSize, backgroundColor) {
    this.size = drawingSize;
    this.svg = createSVGElement("svg");
    this.backgroundColor = backgroundColor;
    let topLeft = 100 - drawingSize / 2;
    this.svg.setAttribute(
      "viewBox",
      `${topLeft} ${topLeft} ${drawingSize} ${drawingSize}`,
    );
    this.svg.id = elementId;
    this.layers = {};
    this.layers.background = this.svg.appendChild(createSVGElement("g"));
    let bgRect = this.svg.appendChild(createSVGElement("rect"));
    setAttributes(bgRect, {
      x: `${topLeft - 1}`,
      y: `${topLeft - 1}`,
      width: `${drawingSize + 2}`,
      height: `${drawingSize + 2}`,
      fill: this.backgroundColor,
    });
    this.layers.arena = this.svg.appendChild(createSVGElement("g"));
    this.arenaClipPath = this.svg.appendChild(createSVGElement("clipPath"));
    this.arenaClipPath.id = newID("xms-arena-clip-path");
    this.layers.arenaClippedMechanics = this.svg.appendChild(
      createSVGElement("g"),
    );
    this.layers.arenaClippedMechanics.setAttribute(
      "clip-path",
      `url(#${this.arenaClipPath.id})`,
    );
    this.layers.unclippedMechanics = this.svg.appendChild(
      createSVGElement("g"),
    );
    this.layers.enemies = this.svg.appendChild(createSVGElement("g"));
    this.layers.dutySupport = this.svg.appendChild(createSVGElement("g"));
    this.layers.player = this.svg.appendChild(createSVGElement("g"));
    this.layers.partyMemberEffects = this.svg.appendChild(
      createSVGElement("g"),
    );
    this.layers.messages = this.svg.appendChild(createSVGElement("g"));
    document.getElementById(elementId).replaceWith(this.svg);
  }
}

function createSVGElement(name) {
  return document.createElementNS("http://www.w3.org/2000/svg", name);
}

const standardStrokeWidth = 0.25;

export { Drawing, createSVGElement, standardStrokeWidth };
