// rotates around 100, 100 because that's the silly coordinate system we use
function rotatePoint(point, degreesClockwise) {
  let rotated = { x: point.x, y: point.y };
  if (degreesClockwise % 360 != 0) {
    let radians = (2 * degreesClockwise * Math.PI) / 360;
    let cos = Math.cos(radians);
    let sin = Math.sin(radians);
    rotated.x = (point.x - 100) * cos - (point.y - 100) * sin + 100;
    rotated.y = (point.y - 100) * cos + (point.x - 100) * sin + 100;
  }
  return rotated;
}

function distance(start, end) {
  return Math.sqrt(Math.pow(start.x - end.x, 2) + Math.pow(start.y - end.y, 2));
}

function newID(base) {
  let number = 1;
  let id = base.toString() + "-" + number;
  while (document.getElementById(id)) {
    number++;
    id = base.toString() + number;
  }
  return id;
}

function setAttributes(node, attributes) {
  for (let name in attributes) {
    if (attributes[name] === null || attributes[name] === undefined) {
      node.removeAttribute(name);
    } else {
      node.setAttribute(name, attributes[name]);
    }
  }
}

function blurNumber(number, max_amount) {
  return number - max_amount + 2 * max_amount * Math.random();
}

export { rotatePoint, distance, newID, setAttributes, blurNumber };
