import { theoreticalTicksPerSecond } from "./constants.js";
import {
  initializeInput,
  updateInput,
  clearButtonPresses,
  wasMenuPressed,
  wasResetPressed,
} from "./input.js";
import { Drawing } from "./drawing.js";
import { Party } from "./party.js";
import { RestartInfo } from "./info_displays.js";
import { Entities } from "./entities.js";
import { Timeline } from "./timeline.js";

/* @license
License for the xiv-mech-sim library

Copyright 2023 Jen Pollock

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

class Sim {
  #intervalId;
  #running;
  #drawingId;
  #drawingSize;
  #menu;
  #lastUpdateTime;
  #initFn;

  constructor(options) {
    this.#drawingId = options.placeholderId;
    this.#drawingSize = options.drawingSize;
    this.backgroundColor = options.backgroundColor;
    this.#menu = document.getElementById(options.menuId);
    this.entities = null;
    this.timeline = null;
    this.#intervalId = null;
    this.#running = false;
    this.#lastUpdateTime = 0;
    this.#initFn = options.initFn;
  }

  run() {
    initializeInput();
    document.getElementById("start").addEventListener("click", (evt) => {
      this.startPull();
    });
    this.#menu.addEventListener("cancel", (evt) => {
      // can't see a good way to prevent closing menu with escape, so just
      // treat it like the user clicked start, it'll be fine probably
      this.startPull();
    });
    this.#menu.showModal();
  }

  startPull() {
    this.drawing = new Drawing(
      this.#drawingId,
      this.#drawingSize,
      this.backgroundColor,
    );
    this.party = new Party(() => this.endRun());
    this.entities = new Entities(this);
    this.timeline = new Timeline();
    this.entities.add(this.timeline);

    this.#initFn(this);

    for (let partyMember of this.party.list) {
      if (
        document.querySelector(
          `input[type="radio"][name="player"][value="${partyMember.label}"]`,
        ).checked
      ) {
        partyMember.player_controlled = true;
        partyMember.show(); // in case duty support has already been hidden
        partyMember.layer = 2; // magic numbers: they're terrible but also less initial work
        this.player = partyMember;
      }
      this.entities.add(partyMember);
    }
    if (this.#intervalId !== null) {
      clearInterval(this.#intervalId);
    }
    this.#lastUpdateTime = Date.now();
    this.#intervalId = setInterval(
      () => this.update(),
      1000 / theoreticalTicksPerSecond,
    );

    this.timeline.start();
    this.#running = true;
  }

  update() {
    let suspendHandle = this.drawing.svg.suspendRedraw(
      1000 / theoreticalTicksPerSecond,
    );
    let time = Date.now();
    let msSinceLastUpdate = time - this.#lastUpdateTime;
    this.#lastUpdateTime = time;
    updateInput();
    if (this.#running) {
      this.entities.update(this, msSinceLastUpdate);
    } else if (!this.#menu.open) {
      if (wasMenuPressed()) {
        this.#menu.showModal();
      } else if (wasResetPressed()) {
        this.startPull();
      }
    }
    this.drawing.svg.unsuspendRedraw(suspendHandle);
  }

  endRun() {
    this.#running = false;
    this.entities.add(new RestartInfo(this.backgroundColor));
    clearButtonPresses();
  }

  allowMovement(from, to) {
    return this.entities.allowMovement(from, to);
  }
}

export { Sim };
