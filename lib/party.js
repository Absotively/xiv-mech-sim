import { Entity } from "./entities.js";
import { partyMemberSpeedInYalmsPerSecond } from "./constants.js";
import { getMovementUnitVector } from "./input.js";
import { createSVGElement, standardStrokeWidth } from "./drawing.js";

class Party {
  constructor(KO_callback) {
    this.T1 = new PartyMember(
      "T1",
      "rgb(0,66,197)",
      1,
      false,
      false,
      KO_callback,
    );
    this.H1 = new PartyMember(
      "H1",
      "rgb(44,197,44)",
      1,
      false,
      true,
      KO_callback,
    );
    this.M1 = new PartyMember("M1", "#f00", 1, true, false, KO_callback);
    this.R1 = new PartyMember("R1", "#f00", 1, true, true, KO_callback);
    this.T2 = new PartyMember(
      "T2",
      "rgb(0,66,197)",
      2,
      false,
      false,
      KO_callback,
    );
    this.H2 = new PartyMember(
      "H2",
      "rgb(44,197,44)",
      2,
      false,
      true,
      KO_callback,
    );
    this.M2 = new PartyMember("M2", "#f00", 2, true, false, KO_callback);
    this.R2 = new PartyMember("R2", "#f00", 2, true, true, KO_callback);
    this.list = [
      this.T1,
      this.H1,
      this.M1,
      this.R1,
      this.T2,
      this.H2,
      this.M2,
      this.R2,
    ];
  }

  reset() {
    for (let pm of this.list) {
      pm.reset();
    }
  }

  hideDutySupport() {
    for (let pm of this.list) {
      if (!pm.player_controlled) {
        pm.hide();
      }
    }
  }

  showDutySupport() {
    for (let pm of this.list) {
      pm.show();
    }
  }
}

const party_member_size = 2.5;
const party_member_text_size = 0.7 * party_member_size;
const x_half_size = party_member_size / 2;
const x_corner_offset = x_half_size / 5;
const x_path_spec = `
  M${x_corner_offset - x_half_size},-${x_half_size}
  L0,-${x_corner_offset}
  ${x_half_size - x_corner_offset},-${x_half_size}
  ${x_half_size},${x_corner_offset - x_half_size}
  ${x_corner_offset},0
  ${x_half_size},${x_half_size - x_corner_offset}
  ${x_half_size - x_corner_offset},${x_half_size}
  0,${x_corner_offset}
  ${x_corner_offset - x_half_size},${x_half_size}
  -${x_half_size},${x_half_size - x_corner_offset}
  -${x_corner_offset},0
  -${x_half_size},${x_corner_offset - x_half_size}
  Z`.replaceAll(/\s+/g, " ");

class PartyMember extends Entity {
  #svgGroup;
  #hidden;

  constructor(label, color, lp, is_dps, is_ranged, KO_callback) {
    super(1);
    this.label = label;
    this.color = color;
    this.KOd = false;
    this.isActive = true;
    this.lp = lp;
    this.is_dps = is_dps;
    this.is_ranged = is_ranged;
    this.KO_callback = KO_callback;
    this.target_positions = [];
    this.player_controlled = false;
    this.#svgGroup = null;
  }

  reset() {
    this.target_positions = [];
    this.player_controlled = false;
    this.layer = 1;
    this.KOd = false;
    this.isActive = true;
    this.removeFromDrawing();
  }

  set_position(x, y) {
    this.x = x;
    this.y = y;
  }

  set_target_positions(list) {
    this.target_positions = list;
  }

  add_target_position(x, y) {
    this.target_positions.push({ x: x, y: y });
  }

  ko() {
    this.KOd = true;
    if (this.#svgGroup) {
      let x_path = createSVGElement("path");
      x_path.setAttribute("d", x_path_spec);
      x_path.setAttribute("fill", "rgba(77,0,0,0.8)");
      this.#svgGroup.append(x_path);
    }
  }

  update(sim, msSinceLastUpdate) {
    if (this.KOd) {
      if (this.isActive) {
        this.isActive = false;
        this.KO_callback();
      }
      return;
    }
    let speed = (partyMemberSpeedInYalmsPerSecond * msSinceLastUpdate) / 1000;
    if (this.player_controlled) {
      let v = getMovementUnitVector();
      let new_x = this.x + v.x * speed;
      let new_y = this.y + v.y * speed;
      if (sim.allowMovement({ x: v.x, y: v.y }, { x: new_x, y: new_y })) {
        this.x = new_x;
        this.y = new_y;
      }
    } else {
      let movement_remaining = speed;
      while (this.target_positions.length > 0 && movement_remaining > 0) {
        let next_target_x_distance = this.target_positions[0].x - this.x;
        let next_target_y_distance = this.target_positions[0].y - this.y;
        let next_target_distance = Math.sqrt(
          next_target_x_distance * next_target_x_distance +
            next_target_y_distance * next_target_y_distance,
        );
        if (next_target_distance <= movement_remaining) {
          this.x = this.target_positions[0].x;
          this.y = this.target_positions[0].y;
          movement_remaining -= next_target_distance;
          this.target_positions.shift();
        } else {
          this.x +=
            (movement_remaining * next_target_x_distance) /
            next_target_distance;
          this.y +=
            (movement_remaining * next_target_y_distance) /
            next_target_distance;
          movement_remaining = 0;
        }
      }
    }
    if (this.#svgGroup !== null) {
      this.#svgGroup.setAttribute(
        "transform",
        `translate(${this.x}, ${this.y})`,
      );
    }
  }

  addToDrawing(drawing) {
    this.#svgGroup = createSVGElement("g");
    if (this.#hidden) {
      this.#svgGroup.setAttribute("opacity", 0);
    }
    let circle = createSVGElement("circle");
    circle.setAttribute("cx", "0");
    circle.setAttribute("cy", "0");
    circle.setAttribute("r", `${party_member_size / 2}`);
    circle.setAttribute("fill", this.color);
    circle.setAttribute("stroke-width", standardStrokeWidth);
    circle.setAttribute("stroke", this.player_controlled ? "#fff" : "#000");
    this.#svgGroup.append(circle);
    let text = createSVGElement("text");
    text.textContent = this.label;
    text.setAttribute("dominant-baseline", "central");
    text.setAttribute("text-anchor", "middle");
    text.setAttribute("x", "0");
    text.setAttribute("y", "0");
    text.setAttribute("fill", "#fff");
    text.setAttribute("font-family", "monospace");
    text.setAttribute("font-size", `${party_member_text_size}`);
    this.#svgGroup.append(text);
    this.#svgGroup.setAttribute("transform", `translate(${this.x}, ${this.y})`);
    if (this.player_controlled) {
      drawing.layers.player.append(this.#svgGroup);
    } else {
      drawing.layers.dutySupport.append(this.#svgGroup);
    }
  }

  removeFromDrawing() {
    if (this.#svgGroup !== null) {
      this.#svgGroup.remove();
      this.#svgGroup = null;
    }
  }

  hide() {
    if (this.#svgGroup !== null) {
      this.#svgGroup.setAttribute("opacity", 0);
    }
    this.#hidden = true;
  }

  show() {
    if (this.#svgGroup !== null) {
      this.#svgGroup.removeAttribute("opacity");
    }
    this.#hidden = false;
  }
}

export { Party };
