import { Sim } from "xiv-mech-sim";
import { Success } from "xiv-mech-sim/info_displays.js";
import { RoundArena } from "xiv-mech-sim/arena.js";
import { blurNumber } from "xiv-mech-sim/utilities.js";

// initFn and the things it's calling are organized the way I usually
// do them, but you can absolutely replace its contents entirely
let sim = new Sim({
  placeholderId: "sim",
  drawingSize: 50, // yalms!
  backgroundColor: "#00072b",
  menuId: "menu",
  initFn: function (sim) {
    rollRNG(sim);
    sim.entities.add(new RoundArena(sim, 40, "#b0befc"));
    setUpMechanics(sim);
    setUpDutySupport(sim);
  },
});

sim.run();

let rngChoices = {};

function rollRNG(sim) {
  // set up rng here

  console.log(JSON.stringify(rngChoices));
}

// all times are in milliseconds
const succeed = 10000;

function setUpMechanics(sim) {
  sim.timeline.addEvent(succeed, () => {
    sim.entities.add(new Success());
    sim.endRun();
  });
}

function setUpDutySupport(sim) {
  let party = sim.party.list;

  // randomize starting positions
  for (let pm of party) {
    pm.set_position(blurNumber(100, 5), blurNumber(100, 5));
  }
}
