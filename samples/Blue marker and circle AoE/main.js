import { Sim } from "xiv-mech-sim";
import { SquareArena } from "xiv-mech-sim/arena.js";
import { BlueMarker, Success } from "xiv-mech-sim/info_displays.js";
import { CircleAoE } from "xiv-mech-sim/mechanics.js";

let sim = new Sim({
  placeholderId: "sim",
  drawingSize: 40, // yalms!
  backgroundColor: "rgb(25,25,25)",
  menuId: "menu",
  initFn: function (sim) {
    rollRNG(sim);
    sim.entities.add(new SquareArena(sim, 30, "rgb(125,125,125)"));
    setUpMechanics(sim);
    setUpDutySupport(sim);
  },
});

sim.run();

let rngChoices = {};

function rollRNG(sim) {
  let playerLabel = document.querySelector(
    `input[type="radio"][name="player"]:checked`,
  ).value;
  if (
    playerLabel != "None" &&
    document.querySelector('input[name="target-player"]').checked
  ) {
    for (let pm of sim.party.list) {
      if (pm.label == playerLabel) {
        rngChoices.targetPartyMember = pm;
      }
    }
  } else {
    rngChoices.targetPartyMember =
      sim.party.list[Math.floor(Math.random() * 8)];
  }
  rngChoices.secondAoENorth = Math.random() > 0.5;

  console.log(JSON.stringify(rngChoices));
}

// all times are in milliseconds
const show_blue_marker = 2000;
const first_circle_hits = 12000;
const second_circle_warning = 14000;
const second_circle_hits = 23000;
const succeed = 23500;

const hit_indicator_time = 1000;

function setUpMechanics(sim) {
  let blueMarker = new BlueMarker(rngChoices.targetPartyMember);
  let circle1 = new CircleAoE({
    target: rngChoices.targetPartyMember,
    radius: 20,
    warning_color: "rgba(255,255,255,0.7",
  });
  let circle2 = new CircleAoE({
    target: { x: 100, y: rngChoices.secondAoENorth ? 85 : 115 },
    radius: 15,
    warning_color: "rgba(240,240,255,0.7",
  });

  sim.timeline.addEvent(show_blue_marker, () => {
    sim.entities.add(blueMarker);
  });

  sim.timeline.addEvent(first_circle_hits, () => {
    sim.entities.remove(blueMarker);
    sim.entities.add(circle1);
    circle1.hit(sim.party.list);
  });

  sim.timeline.addEvent(first_circle_hits + hit_indicator_time, () => {
    sim.entities.remove(circle1);
  });

  sim.timeline.addEvent(second_circle_warning, () => {
    sim.entities.add(circle2);
  });

  sim.timeline.addEvent(second_circle_hits, () => {
    circle2.hit(sim.party.list);
  });

  sim.timeline.addEvent(second_circle_hits + hit_indicator_time, () => {
    sim.entities.remove(circle2);
  });

  sim.timeline.addEvent(succeed, () => {
    sim.entities.add(new Success());
    sim.endRun();
  });
}

const thinking_time = 1500;

function setUpDutySupport(sim) {
  let party = sim.party.list;

  // randomize starting positions
  for (let pm of party) {
    pm.set_position(blur(100, 14), blur(100, 14));
  }

  sim.timeline.addEvent(show_blue_marker + thinking_time, () => {
    for (let pm of party) {
      if (Object.is(pm, rngChoices.targetPartyMember)) {
        pm.set_target_positions([{ x: blur(100, 0.5), y: blur(86, 0.5) }]);
      } else {
        pm.set_target_positions([{ x: blur(pm.x, 0.5), y: blur(113, 1.5) }]);
      }
    }
  });

  sim.timeline.addEvent(second_circle_warning + thinking_time, () => {
    let ideal_y = rngChoices.secondAoENorth ? 113 : 87;
    for (let pm of party) {
      pm.set_target_positions([{ x: blur(pm.x, 0.5), y: blur(ideal_y, 1.5) }]);
    }
  });
}

function blur(number, max_amount) {
  return number - max_amount + 2 * max_amount * Math.random();
}
