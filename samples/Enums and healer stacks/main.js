import { Sim } from "xiv-mech-sim";
import { SquareArena } from "xiv-mech-sim/arena.js";
import { Success } from "xiv-mech-sim/info_displays.js";
import { Enums, LPStacks } from "xiv-mech-sim/mechanics.js";

let sim = new Sim({
  placeholderId: "sim",
  drawingSize: 40, // yalms!
  backgroundColor: "rgb(0,0,0)",
  menuId: "menu",
  initFn: function (sim) {
    rollRNG(sim);
    sim.entities.add(new SquareArena(sim, 30, "rgb(25,25,25)"));
    setUpMechanics(sim);
    setUpDutySupport(sim);
  },
});

sim.run();

let rngChoices = {};

function rollRNG(sim) {
  rngChoices.enumsFirst = Math.random() > 0.5;
  rngChoices.enumsTargetDPS = Math.random() > 0.5;

  console.log(JSON.stringify(rngChoices));
}

// all times are in milliseconds
const first_stacks_show = 2000;
const first_stacks_hit = 12000;
const second_stacks_show = 14000;
const second_stacks_hit = 20000;
const succeed = 20500;

const hit_indicator_time = 1000;

function setUpMechanics(sim) {
  let enums = new Enums(sim, rngChoices.enumsTargetDPS);
  let lp_stacks = new LPStacks(sim);
  let first_stacks = rngChoices.enumsFirst ? enums : lp_stacks;
  let second_stacks = rngChoices.enumsFirst ? lp_stacks : enums;
  sim.timeline.addEvent(first_stacks_show, () => {
    sim.entities.add(first_stacks);
  });
  sim.timeline.addEvent(first_stacks_hit, () => {
    first_stacks.hit(sim.party.list);
  });
  sim.timeline.addEvent(first_stacks_hit + hit_indicator_time, () => {
    sim.entities.remove(first_stacks);
  });
  sim.timeline.addEvent(second_stacks_show, () => {
    sim.entities.add(second_stacks);
  });
  sim.timeline.addEvent(second_stacks_hit, () => {
    second_stacks.hit(sim.party.list);
  });
  sim.timeline.addEvent(second_stacks_hit + hit_indicator_time, () => {
    sim.entities.remove(second_stacks);
  });
  sim.timeline.addEvent(succeed, () => {
    sim.entities.add(new Success());
    sim.endRun();
  });
}

const thinking_time = 1500;

function setUpDutySupport(sim) {
  let party = sim.party.list;

  // randomize starting positions
  for (let pm of party) {
    pm.set_position(blur(100, 14), blur(100, 14));
  }

  function positionForHealerStacks() {
    for (let pm of party) {
      if (pm.lp == 1) {
        pm.set_target_positions([{ x: blur(92, 1), y: blur(100, 1) }]);
      } else {
        pm.set_target_positions([{ x: blur(108, 1), y: blur(100, 1) }]);
      }
    }
  }

  function positionForEnums() {
    for (let pm of party) {
      if (pm.lp == 1) {
        if (pm.is_ranged) {
          pm.set_target_positions([{ x: blur(88, 1), y: blur(100, 1) }]);
        } else {
          pm.set_target_positions([{ x: blur(96, 1), y: blur(100, 1) }]);
        }
      } else {
        if (pm.is_ranged) {
          pm.set_target_positions([{ x: blur(112, 1), y: blur(100, 1) }]);
        } else {
          pm.set_target_positions([{ x: blur(104, 1), y: blur(100, 1) }]);
        }
      }
    }
  }

  sim.timeline.addEvent(first_stacks_show + thinking_time, () => {
    if (rngChoices.enumsFirst) {
      positionForEnums();
    } else {
      positionForHealerStacks();
    }
  });

  sim.timeline.addEvent(second_stacks_show + thinking_time, () => {
    if (rngChoices.enumsFirst) {
      positionForHealerStacks();
    } else {
      positionForEnums();
    }
  });
}

function blur(number, max_amount) {
  return number - max_amount + 2 * max_amount * Math.random();
}
