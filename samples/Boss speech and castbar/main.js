import { Sim } from "xiv-mech-sim";
import { SquareArena } from "xiv-mech-sim/arena.js";
import {
  Hitbox,
  BossSpeech,
  Castbar,
  Success,
} from "xiv-mech-sim/info_displays.js";

let sim = new Sim({
  placeholderId: "sim",
  drawingSize: 40, // yalms!
  backgroundColor: "rgb(0,0,0)",
  menuId: "menu",
  initFn: function (sim) {
    rollRNG(sim);
    sim.entities.add(new SquareArena(sim, 30, "rgb(25,25,25)"));
    setUpMechanics(sim);
    setUpDutySupport(sim);
  },
});

sim.run();

let rngChoices = {};

function rollRNG(sim) {
  console.log(JSON.stringify(rngChoices));
}

// all times are in milliseconds
const show_speech = 1000;
const start_castbar = 4000;
const remove_speech = 5000;
const castbar_duration = 4000;
const succeed = 9000;

function setUpMechanics(sim) {
  sim.entities.add(new Hitbox(10));
  let speech = new BossSpeech("How very glib.");
  let castbar = new Castbar("Terminus Est", castbar_duration);
  sim.timeline.addEvent(show_speech, () => {
    sim.entities.add(speech);
  });
  sim.timeline.addEvent(start_castbar, () => {
    sim.entities.add(castbar);
  });
  sim.timeline.addEvent(remove_speech, () => {
    sim.entities.remove(speech);
  });
  sim.timeline.addEvent(start_castbar + castbar_duration, () => {
    sim.entities.remove(castbar);
  });
  sim.timeline.addEvent(succeed, () => {
    sim.entities.add(new Success());
    sim.endRun();
  });
}

function setUpDutySupport(sim) {
  let party = sim.party.list;

  // randomize starting positions
  for (let pm of party) {
    pm.set_position(blur(100, 14), blur(100, 14));
  }
}

function blur(number, max_amount) {
  return number - max_amount + 2 * max_amount * Math.random();
}
