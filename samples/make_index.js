import * as fs from "node:fs";
import * as path from "node:path";
import { fileURLToPath } from "url";

const __dirname = path.dirname(fileURLToPath(import.meta.url));
let indexFilename = path.join(__dirname, "index.html");
let indexFile = fs.openSync(indexFilename, "w");

fs.writeSync(
  indexFile,
  "<!DOCTYPE html>\n<head><title>xiv-mech-sim samples</title></head>\n<body>\n<h1>xiv-mech-sim samples</h1>\n<ul>\n",
);

for (const dirent of fs.readdirSync(__dirname, { withFileTypes: true })) {
  if (dirent.isDirectory()) {
    fs.writeSync(
      indexFile,
      `<li><a href="${dirent.name}/">${dirent.name}</a></li>\n`,
    );
  }
}

fs.writeSync(indexFile, "</ul>\n</body>\n</html>");

fs.closeSync(indexFile);
