import { Sim } from "xiv-mech-sim";
import { RoundArena } from "xiv-mech-sim/arena.js";
import { Success } from "xiv-mech-sim/info_displays.js";
import { DonutAoE } from "xiv-mech-sim/mechanics.js";
import { rotatePoint, distance } from "xiv-mech-sim/utilities.js";

let sim = new Sim({
  placeholderId: "sim",
  drawingSize: 40, // yalms!
  backgroundColor: "rgb(0,0,0)",
  menuId: "menu",
  initFn: function (sim) {
    rollRNG();
    sim.entities.add(new RoundArena(sim, 30, "rgb(25,25,25)"));
    setUpMechanics(sim);
    setUpDutySupport(sim);
  },
});

let rngChoices = {};

function rollRNG(sim) {
  rngChoices.donutLocationAngle = Math.random() * 360;

  console.log(JSON.stringify(rngChoices));
}

sim.run();

// all times are in milliseconds
const donut_warning = 5000;
const donut_hits = 15000;
const succeed = 15500;

function setUpMechanics(sim) {
  let donut = new DonutAoE({
    target: rotatePoint({ x: 100, y: 85 }, rngChoices.donutLocationAngle),
    inner_radius: 10,
    outer_radius: 28,
  });
  sim.timeline.addEvent(donut_warning, () => {
    sim.entities.add(donut);
  });
  sim.timeline.addEvent(donut_hits, () => {
    donut.hit(sim.party.list);
  });

  sim.timeline.addEvent(succeed, () => {
    sim.entities.add(new Success());
    sim.endRun();
  });
}

function setUpDutySupport(sim) {
  let party = sim.party.list;

  let dutySupport = document.querySelector('[name="duty-support"]').checked;
  if (!dutySupport) {
    sim.party.hideDutySupport();
  }

  // randomize starting positions
  for (let pm of party) {
    let x = blur(100, 14);
    let y = blur(100, 14);
    while (distance({ x: x, y: y }, { x: 100, y: 100 }) > 15) {
      x = blur(100, 14);
      y = blur(100, 14);
      // look this could take forever but the odds of that are very low,
      // it'll be fine
    }
    pm.set_position(x, y);
  }

  let safe_spot = rotatePoint({ x: 100, y: 90 }, rngChoices.donutLocationAngle);
  sim.timeline.addEvent(donut_warning + 1500, () => {
    for (let pm of party) {
      pm.set_target_positions([
        { x: blur(safe_spot.x, 2.5), y: blur(safe_spot.y, 2.5) },
      ]);
    }
  });
}

function blur(number, max_amount) {
  return number - max_amount + 2 * max_amount * Math.random();
}
