import { Sim } from "xiv-mech-sim";
import { SquareArena } from "xiv-mech-sim/arena.js";
import { Success } from "xiv-mech-sim/info_displays.js";

let sim = new Sim({
  placeholderId: "sim",
  drawingSize: 40, // yalms!
  backgroundColor: "rgb(0,0,0)",
  menuId: "menu",
  initFn: function (sim) {
    sim.entities.add(new SquareArenaWithSolidWalls(sim, 30, "rgb(25,25,25)"));
    setUpMechanics(sim);
    setUpDutySupport(sim);
  },
});

sim.run();

// all times are in milliseconds
const succeed = 30000;

function setUpMechanics(sim) {
  sim.timeline.addEvent(succeed, () => {
    sim.entities.add(new Success());
    sim.endRun();
  });
}

function setUpDutySupport(sim) {
  let party = sim.party.list;

  // randomize starting positions
  for (let pm of party) {
    pm.set_position(blur(100, 14), blur(100, 14));
  }
}

function blur(number, max_amount) {
  return number - max_amount + 2 * max_amount * Math.random();
}

class SquareArenaWithSolidWalls extends SquareArena {
  #left;
  #right;
  #top;
  #bottom;
  constructor(sim, size, color) {
    super(sim, size, color);
    this.#left = this.#top = 100 - size / 2;
    this.#right = this.#bottom = 100 + size / 2;
  }

  update() {
    // override SquareArena's wall-of-death update function
    // and instead do nothing
  }

  allowMovement(from, to) {
    if (
      to.x <= this.#left ||
      to.x >= this.#right ||
      to.y <= this.#top ||
      to.y >= this.#right
    ) {
      return false;
    } else {
      return true;
    }
  }
}
