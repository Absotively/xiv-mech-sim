import { Sim } from "xiv-mech-sim";
import { SquareArena } from "xiv-mech-sim/arena.js";
import { LCNumbers, Success } from "xiv-mech-sim/info_displays.js";

let sim = new Sim({
  placeholderId: "sim",
  drawingSize: 40, // yalms!
  backgroundColor: "rgb(0,0,0)",
  menuId: "menu",
  initFn: function (sim) {
    rollRNG(sim);
    sim.entities.add(new SquareArena(sim, 30, "rgb(25,25,25)"));
    setUpMechanics(sim);
    setUpDutySupport(sim);
  },
});

sim.run();

let rngChoices = {};

function rollRNG(sim) {
  let toShuffle = sim.party.list.slice();
  rngChoices.targetsToNumbersMap = new Map();
  for (let i = 1; i <= 8; i++) {
    let selected = Math.floor(Math.random() * toShuffle.length);
    rngChoices.targetsToNumbersMap.set(toShuffle[selected], i);
    toShuffle.splice(selected, 1);
  }

  console.log(JSON.stringify(rngChoices));
}

// all times are in milliseconds
const show_lc_numbers = 1000;
const remove_lc_numbers = 4000;
const succeed = 5000;

function setUpMechanics(sim) {
  let lcNumbers = new LCNumbers(rngChoices.targetsToNumbersMap);
  sim.timeline.addEvent(show_lc_numbers, () => {
    sim.entities.add(lcNumbers);
  });
  sim.timeline.addEvent(remove_lc_numbers, () => {
    sim.entities.remove(lcNumbers);
  });
  sim.timeline.addEvent(succeed, () => {
    sim.entities.add(new Success());
    sim.endRun();
  });
}

function setUpDutySupport(sim) {
  let party = sim.party.list;

  // randomize starting positions
  for (let pm of party) {
    pm.set_position(blur(100, 14), blur(100, 14));
  }
}

function blur(number, max_amount) {
  return number - max_amount + 2 * max_amount * Math.random();
}
